FROM --platform=$TARGETPLATFORM php:8.1-fpm

RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
RUN curl -fsSL https://deb.nodesource.com/setup_lts.x | bash -
RUN apt-get -y update \ 
&& apt-get install -y libicu-dev nodejs \ 
&& docker-php-ext-configure intl \ 
&& docker-php-ext-install intl


# Set working directory
WORKDIR /app/html

# Copy existing application directory contents
COPY . /app/html

# Install Laravel dependencies
RUN composer install
RUN mkdir storage/app/images; \
    mkdir storage/app/chunks;\
    chmod 777 -R storage
RUN npm install; \
    npm run build
RUN cp .env.prod .env

RUN set -eux; \
	\
    addgroup --system --gid 101 nginx && \
    adduser --system --disabled-login --ingroup nginx --no-create-home --home /nonexistent --gecos "nginx user" --shell /bin/false --uid 101 nginx; \
    chown -R nginx:nginx /app/html; \
    sed -i 's/www-data/nginx/g' /usr/local/etc/php-fpm.d/www.conf; \
    cp /usr/local/etc/php/php.ini-production /usr/local/etc/php/php.ini

